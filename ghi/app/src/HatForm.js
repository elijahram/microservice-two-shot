import { useState, useEffect } from "react";

function HatsForm() {
  const [locations, setLocations] = useState([]);
  const [formData, setFormData] = useState({
    fabric: '',
    style_name: '',
    color: '',
    picture_url: '',
    location: '',
  })


  const fetchData = async () => {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    

    const hatsUrl = "http://localhost:8090/api/hats/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(hatsUrl, fetchOptions);
    if (response.ok) {
        setFormData({
            fabric: '',
            style_name: '',
            color: '',
            picture_url: '',
            location: '',
        })
    }
  };

  const handleChange = (event) => {
    setFormData({
        ...formData,
        [event.target.name]: event.target.value
    })
  }





  return (
    <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input value={formData.fabric} onChange={handleChange} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={formData.style_name} onChange={handleChange} placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control" />
                  <label htmlFor="color">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={formData.color} onChange={handleChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={formData.picture_url} onChange={handleChange} placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                  <label htmlFor="picture_url">Picture</label>
                </div>
                <div className="mb-3">
                  <select value={formData.location} onChange={handleChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.href} value={location.href}>
                          {location.closet_name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>







  )










}



export default HatsForm;