import React, { useState, useEffect } from "react";

function CreateShoeForm() {
  const [bins, setBins] = useState([]);

  const [formData, setFormData] = useState({
    model_name: "",
    manufacturer: "",
    color: "",
    picture_url: "",
    bin: "",
  });

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/bins/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

      setFormData({
        model_name: "",
        manufacturer: "",
        color: "",
        picture_url: "",
      });
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                placeholder="Model Name"
                required
                type="text"
                value={formData.model_name}
                name="model_name"
                id="model_name"
                className="form-control"
              />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                placeholder="Manufacturer"
                required
                type="text"
                value={formData.manufacturer}
                name="manufacturer"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                placeholder="Color"
                required
                type="text"
                value={formData.color}
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                placeholder="Picture Url"
                required
                type="text"
                value={formData.picture_url}
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleChange}
                required
                value={formData.bin}
                name="bin"
                id="bin"
                className="form-select"
              >
                <option value="">Choose a Bin</option>
                {bins.map((bin) => {
                  return (
                    <option key={bin.href} value={bin.href}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateShoeForm;
