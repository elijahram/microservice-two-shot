import React, { useState, useEffect } from "react";

function HatsList(props) {
  const [hatList, setHatList] = useState([]);
  const fetchData = async () => {
    const url = "http://localhost:8090/api/hats/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setHatList(data);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (id) => {
    const url = `http://localhost:8090/api/hats/${id}/`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      fetchData();
    } else {
      alert("Hat was not deleted!");
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Fabric</th>
          <th>Style Name</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {hatList.map((hat) => {
          return (
            <tr key={hat.id}>
              <td>{hat.fabric}</td>
              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td>{hat.picture_url}</td>
              <td>{hat.location.closet_name}</td>
              <td>
                <button
                  className="btn btn-secondary"
                  onClick={() => handleDelete(hat.id)}
                >
                  DELETE
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
