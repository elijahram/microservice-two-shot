function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have the solution for
          you!
        </p>
        <img
          src="https://closettecnj.com/wp-content/uploads/2020/06/shoe-closet-gallery-14-scaled.jpg"
          class="rounded mx-auto d-block"
          width="100%"
          height="50%"
          alt="shoes"
        />
      </div>
    </div>
  );
}

export default MainPage;
