import React, { useState, useEffect } from "react";

const ShoesList = () => {
  const [shoeList, setShoeList] = useState([]);
  const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setShoeList(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (id) => {
    const url = `http://localhost:8080/api/shoes/${id}/`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      fetchData();
    } else {
      alert("Shoe was not deleted!");
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Model Name</th>
          <th>Manufacturer</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Bin</th>
          <th>Option to Delete</th>
        </tr>
      </thead>
      <tbody>
        {shoeList.map((shoe) => {
          return (
            <tr key={shoe.id}>
              <td>{shoe.model_name}</td>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.color}</td>
              <td>{shoe.picture_url}</td>
              <td>{shoe.bin.closet_name}</td>
              <td>
                <button
                  className="btn btn-secondary"
                  onClick={() => handleDelete(shoe.id)}
                >
                  DELETE
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default ShoesList;
