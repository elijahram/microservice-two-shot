import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoesList from "./ShoesList";
import CreateShoeForm from "./CreateShoeForm";
import HatsList from "./HatList";
import HatsForm from "./HatForm";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path="/shoes/new" element={<CreateShoeForm />} />
          <Route path="hats" element={<HatsList hats={props.hats} />}>
            {" "}
          </Route>
          <Route path="hats/new" element={<HatsForm />}>
            {" "}
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
