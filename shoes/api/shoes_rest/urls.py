from django.urls import path
from .views import list_shoes, delete_shoe

urlpatterns = [
    path('shoes/', list_shoes, name="create_shoes"),
    path('shoes/<int:id>/', delete_shoe, name="delete_shoe"),
]
