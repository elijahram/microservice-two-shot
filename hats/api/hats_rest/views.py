from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
        "location"
        
    ]
    encoders = {"location": LocationVOEncoder()}

    # def get_extra_data(self, o):
    #     return {"location": o.location.name}




# class HatDetailEncoder(ModelEncoder):
#     model = Hats
#     properties = [
#         "fabric",
#         "style_name",
#         "color",
#         "picture_url",
#         "location",
#         "id"
#     ]
#     encoders = {
#         "location": LocationVOEncoder(),
#     }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            hats,
            encoder=HatListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        print(LocationVO.objects.all())

        try:
            href = content["location"]
            location = LocationVO.objects.get(import_href=href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location Id"},
                status=400,
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            {"hat": hat},
            encoder=HatListEncoder,
            safe=False
        )
 

def delete_hat(request, id):
    if request.method == "DELETE":
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


    
    



    