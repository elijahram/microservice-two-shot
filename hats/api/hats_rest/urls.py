from django.urls import path
from .views import api_list_hats, delete_hat


urlpatterns = [
    path("hats/", api_list_hats, name="api_create_hats"),
    path("hats/<int:id>/", delete_hat, name="api_show_hats"),
]

